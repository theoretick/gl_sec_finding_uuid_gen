package main

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/google/uuid"

	report "gitlab.com/gitlab-org/security-products/analyzers/report"
)

const (
  // https://gitlab.com/gitlab-org/gitlab/-/blob/v15.10.1-ee/lib/gitlab/uuid.rb
	GlProdNamespaceID = "58dc0f06-936c-43b3-93bb-71693f1b6570"
	GlDevNamespaceID  = "a143e9e2-41b3-47bc-9a19-081d089229f4"
)

type comparisonReport struct {
	UUIDs []uuid.UUID `json:"uuids"`
	report.Report
}

func ProjectID() string {
	return os.Getenv("PROJECT_ID")
}

func namespaceUUID() string {
	if os.Getenv("RAILS_ENV") == "production" {
		return GlProdNamespaceID
	}

	return GlDevNamespaceID
}

func locationFingerprint(location report.Location) string {
	lineEnd := ""
	if location.LineEnd != 0 {
		lineEnd = fmt.Sprintf("%d", location.LineEnd)
	}

	locationFingerprintData := fmt.Sprintf("%s:%d:%s", location.File, location.LineStart, lineEnd)
	locationFingerprint := sha1.Sum([]byte(locationFingerprintData))

	return hex.EncodeToString(locationFingerprint[:])
}

func identifierFingerprint(ident report.Identifier) string {
	fingerprintData := string(ident.Type) + ":" + string(ident.Value)
	identFingerprint := sha1.Sum([]byte(fingerprintData))

	return hex.EncodeToString(identFingerprint[:])
}

func UUID(uuidData []any) uuid.UUID {
	s := fmt.Sprintf("%s-%s-%s-%s", uuidData...)

	namespaceUUID := uuid.MustParse(namespaceUUID())

	return uuid.NewSHA1(namespaceUUID, []byte(s))
}

func buildReportFrom(input *os.File) comparisonReport {
	outputReport := comparisonReport{}
	r := report.Report{}

	bytes, _ := ioutil.ReadAll(input)
	json.Unmarshal(bytes, &r)

	for idx := range r.Vulnerabilities {
		vuln := r.Vulnerabilities[idx]
		pIdent := vuln.Identifiers[0]
		location := vuln.Location

		uuid5 := UUID([]any{
			r.Scan.Type,
			identifierFingerprint(pIdent),
			locationFingerprint(location),
			ProjectID(),
		})

		outputReport.UUIDs = append(outputReport.UUIDs, uuid5)
	}

	return outputReport
}

func main() {
	var outputReport comparisonReport

	fi, _ := os.Stdin.Stat() // get the FileInfo struct describing the standard input.

	if (fi.Mode() & os.ModeCharDevice) == 0 {
		outputReport = buildReportFrom(os.Stdin)
	} else {
		panic("no input piped")
	}

	for _, result := range outputReport.UUIDs {
		fmt.Println(result)
	}
}
