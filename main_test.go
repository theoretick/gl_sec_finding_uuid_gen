package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/security-products/analyzers/report"
)

const (
	findingIdentifierFingerprint = "35f19579c8ee74552f9a6c88d2d7b881ee14b7b5"
	findingLocationFingerprint   = "47c3c097a40dc47e48932a126872a8448869db56"
	findingUUID                  = "0e5b7fea-b224-55b0-9a87-cdc4d0084b84"
	projectID                    = "33"
)

func TestIdentifierFingerprint(t *testing.T) {
	ident := report.Identifier{
		Type:  "gitleaks_rule_id",
		Value: "gitlab_personal_access_token",
	}

	require.Equal(t, identifierFingerprint(ident), findingIdentifierFingerprint)
}

func TestLocationFingerprint(t *testing.T) {
	location := report.Location{
		File:      "foo.go",
		LineStart: 9,
	}

	require.Equal(t, locationFingerprint(location), findingLocationFingerprint)
}

func TestUUID(t *testing.T) {
	uuid := UUID([]any{
		"secret_detection",
		findingIdentifierFingerprint,
		findingLocationFingerprint,
		projectID,
	}).String()

	require.Equal(t, findingUUID, uuid)
}

func TestBuildReportFrom(t *testing.T) {
	os.Setenv("PROJECT_ID", projectID)
	report, err := os.Open("testdata/gl-secret-detection-report.json")
	require.NoError(t, err)
	defer report.Close()

	cReport := buildReportFrom(report)
	require.Equal(t, 1, len(cReport.UUIDs))
	require.Equal(t, findingUUID, cReport.UUIDs[0].String())
}
