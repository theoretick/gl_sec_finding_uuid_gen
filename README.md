# GL Sec Finding UUID Generator

Simple CLI for generating matching UUIDs to GitLab Rails app. testing against v15.11.0

## Usage

```sh
❯ go build .

❯ export PROJECT_ID=33

❯ cat testdata/gl-secret-detection-report.json | ./finding_uuid_gen
0e5b7fea-b224-55b0-9a87-cdc4d0084b84
```